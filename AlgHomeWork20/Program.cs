﻿string text = "qwe asd zxc abc are abc";
             //0123456789 
string pattern = "abc";

var finiteAutomaton = new FiniteAutomaton(pattern);
int index = finiteAutomaton.Search(text);
Console.WriteLine($"Конечный автомат - {index}");

var indexList = KnuthMorrisPrattAlg.SearchPattern(text, pattern);
Console.WriteLine($"Алгоритма Кнута-Морриса-Пратта - { string.Join(", ", indexList) }");

Console.ReadLine();

public class FiniteAutomaton
{
    private readonly int[,] _transitionTable;    private readonly int _numStates;

    public FiniteAutomaton(string pattern)
    {
        _numStates = pattern.Length + 1;
        _transitionTable = new int[_numStates, 256];

        for (int i = 0; i < _numStates; i++)
        {
            for (int j = 0; j < 256; j++)
            {
                _transitionTable[i, j] = -1;
            }
        }

        for (int i = 0; i < pattern.Length; i++)
        {
            _transitionTable[i, pattern[i]] = i + 1;
        }

        _transitionTable[_numStates - 1, pattern[pattern.Length - 1]] = _numStates - 1;
    }

    public int Search(string text)
    {
        int currentState = 0;
        for (int i = 0; i < text.Length; i++)
        {
            currentState = _transitionTable[currentState, text[i]];
            if (currentState == -1)
            {
                // Если текущее состояние становится -1, переходим к следующему символу в строке
                currentState = 0;
            }
            else if (currentState == _numStates - 1)
            {
                return i - (_numStates - 2);
            }
        }
        return -1;
    }
}



class KnuthMorrisPrattAlg
{
    private static void PrefixFunction(string pattern, int[] prefixFunc)
    {
        prefixFunc[0] = 0;
        int border = 0;
        for (int i = 1; i < pattern.Length; i++)
        {
            while ((border > 0) && (pattern[i] != pattern[border]))
            {
                border = prefixFunc[border - 1];
            }

            if (pattern[i] == pattern[border])
            {
                border++;
            }

            prefixFunc[i] = border;
        }
    }

    public static List<int> SearchPattern(string text, string pattern)
    {
        int pl = pattern.Length;
        int tl = text.Length;
        int[] prefixFunc = new int[pl];
        PrefixFunction(pattern, prefixFunc);
        var resultIndices = new List<int>();

        int j = 0;
        for (int i = 0; i < tl; i++)
        {
            while ((j > 0) && (text[i] != pattern[j]))
            {
                j = prefixFunc[j - 1];
            }

            if (text[i] == pattern[j])
            {
                j++;
            }

            if (j == pl)
            {
                resultIndices.Add(i - (j - 1));
                j = prefixFunc[j - 1];
            }
        }
        
        return resultIndices;
    }
}